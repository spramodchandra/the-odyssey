﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipInput : MonoBehaviour
{
    [SerializeField]
    private bool usingJoystick = true;
    public bool invertPitch = true;

    [Range(-1, 1)]
    public float pitch;
    [Range(-1, 1)]
    public float yaw;
    [Range(-1, 1)]
    public float roll;
    [Range(-1, 1)]
    public float strafe;
    //[Range(0, 1)]
    public float throttle;
    [Range(-1, 1)]
    public float verticalSpeed;
    public float target;
    
    public float threshold = 0.1f;
    public float hyperSpeedExtraTrottle = 4f;
    public float rollBankConstant = 0.5f;
    private bool hyperSpeed = false;

    public bool HyperSpeed()
    {
        return hyperSpeed;
    }

    private bool canControl = false;
    public bool CanControl()
    {
        return canControl;
    }
    // How quickly the throttle reacts to input.
    private float THROTTLE_SPEED = 0.5f;
    private Ship ship;
    private int multiplyerPitch=1;
    public int PitchMultiplier()
    {
        return multiplyerPitch;
    }
    private void Awake()
    {
        ship = GetComponent<Ship>();
    }

    // Update is called once per frame
    void Update()
    {
        canControl = ship.canControl;
        if(invertPitch)
            multiplyerPitch = -1;
        else
            multiplyerPitch = 1;
        if (!usingJoystick)
        {
            yaw = Input.GetAxis("Mouse X");
            pitch = Input.GetAxis("Mouse Y");
            pitch = Mathf.Clamp(pitch, -1.0f, 1.0f);
            yaw = Mathf.Clamp(yaw, -1.0f, 1.0f);
            //SetStickCommandsUsingMouse();
            strafe = Input.GetAxis("Horizontal");
            target = Input.GetAxis("Throttle");
            if (Input.GetKey(KeyCode.C))
                verticalSpeed = 1;
            else if (Input.GetKey(KeyCode.V))
                verticalSpeed = -1;
            else
                verticalSpeed = 0;
            if(Input.GetKey(KeyCode.Tab))
                hyperSpeed = true;
            else
                hyperSpeed = false;
        }
        else
        {
            pitch = Input.GetAxis("LeftJoyStickVertical");
            strafe = Input.GetAxis("RightJoyStickHorizontal");
            yaw = Input.GetAxis("LeftJoyStickHorizontal");
            target = Input.GetAxis("RightJoyStickVertical") * -1;

            yaw = Mathf.Abs(yaw) < threshold ? 0 : yaw;
            pitch = Mathf.Abs(pitch) < threshold ? 0 : pitch;
            pitch = Mathf.Clamp(pitch, -1.0f, 1.0f) * multiplyerPitch;
            yaw = Mathf.Clamp(yaw, -1.0f, 1.0f);


            pitch = pitch > 0 ? 1 : (pitch == 0 ? 0 : -1);
            //yaw = yaw > 0 ? 1 : (yaw == 0 ? 0:-1);
            if (Input.GetButton("ShipUp"))
                verticalSpeed = 1;
            else if (Input.GetButton("ShipDown"))
                verticalSpeed = -1;
            else
                verticalSpeed = 0;
            if (Input.GetButton("JoyStickHyperSpeed"))
                hyperSpeed = true;
            else
                hyperSpeed = false;
        }
        roll = yaw * rollBankConstant;
        if (hyperSpeed && throttle > 0.3)
        {
            target += hyperSpeedExtraTrottle;
            THROTTLE_SPEED = 2f;
        }
        else if (target == -1 || throttle>1)
            THROTTLE_SPEED = 3;
        else
            THROTTLE_SPEED = 0.5f;
        
        throttle = Mathf.MoveTowards(throttle, target, Time.deltaTime * THROTTLE_SPEED);
        throttle = Mathf.Clamp(throttle, 0, 7.0f);
    }


    private void SetStickCommandsUsingMouse()
    {
        Vector3 mousePos = Input.mousePosition;

        // Figure out most position relative to center of screen.
        // (0, 0) is center, (-1, -1) is bottom left, (1, 1) is top right.      
        pitch = (mousePos.y - (Screen.height * 0.5f)) / (Screen.height * 0.5f);
        yaw = (mousePos.x - (Screen.width * 0.5f)) / (Screen.width * 0.5f);

        // Make sure the values don't exceed limits.
        pitch = -Mathf.Clamp(pitch, -1.0f, 1.0f);
        yaw = Mathf.Clamp(yaw, -1.0f, 1.0f);
    }
    private void UpdateKeyboardThrottle(KeyCode increaseKey, KeyCode decreaseKey)
    {
        float target = throttle;

        if (Input.GetKey(increaseKey))
            target = 1.0f;
        else if (Input.GetKey(decreaseKey))
            target = 0.0f;

        throttle = Mathf.MoveTowards(throttle, target, Time.deltaTime * THROTTLE_SPEED);
    }

    public void GetInputs(ref float target, ref float throttle, ref float verticalSpeed, ref float strafe, ref float pitch, ref float yaw , ref bool hyperSpeed)
    {
        target = this.target;
        throttle = this.throttle;
        verticalSpeed = this.verticalSpeed;
        strafe = this.strafe;
        pitch = this.pitch;
        yaw = this.yaw;
        hyperSpeed = this.hyperSpeed;
    }
}
