﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Positron;
public class SecondScene : MonoBehaviour
{
    // Start is called before the first frame update

    public bool reachedLandingPad = false;
    GameObject spaceShip;

    GameObject quad;
    GameObject display;
    private Animator quadAnim;
    ScoreBoard sb;

    private ChairInputWrapper chairWrapper;

    void Start()
    {
        GameObject vm = GameObject.Find("Voyager Manager");
        if (vm != null)
        {
            chairWrapper = vm.GetComponent<ChairInputWrapper>();
            chairWrapper.Yaw_factor = 3f;
            chairWrapper.Pitch_chair_angle = 8f;
            VoyagerDevice.Pitch(new Vector3(chairWrapper.Pitch_chair_angle, chairWrapper.Pitch_vel, chairWrapper.Pitch_acc));
        }
        spaceShip = GameObject.Find("spaceShip");
        foreach (Transform child in GameObject.Find("cockpit").GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "Quad")
            {
                quad = child.gameObject;
                break;
            }
        }

        foreach (Transform child in quad.GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "ForceField")
            {
                display = child.gameObject;
                break;
            }
        }

        quadAnim = quad.GetComponent<Animator>();
        sb = spaceShip.GetComponent<ScoreBoard>();
        string difficulty = PlayerPrefs.GetString("Difficulty");
        TimeBar tb = spaceShip.GetComponent<TimeBar>();
        if (difficulty == "Easy")
        {
            tb.factor = 10;
        }
        else if (difficulty == "Medium")
        {
            tb.factor = 8;
        }
        else if (difficulty == "Hard")
        {
            tb.factor = 5;
        }
    }

    // Update is called once per frame
    Vector3 LandingEndPosition;
    float LandingSpeed;
    float LandingTime = 10f;
    bool startLanding = false;
    float time = 0;
    void Update()
    {
        if (reachedLandingPad)
        {
            sb.endGame();
            spaceShip.GetComponent<Ship>().canControl = false;
            StartCoroutine(StartLanding());
            
            Vector3 start = spaceShip.transform.position;
            /*Vector3 end = GameObject.Find("landingPad").transform.position;
            LandingEndPosition = end;
            LandingEndPosition.y += 8;*/
            LandingEndPosition = GameObject.Find("landingPosition").transform.position;
            LandingSpeed = (start.y - LandingEndPosition.y) / LandingTime;
            Debug.Log(LandingEndPosition);
            Debug.Log(LandingSpeed);
            reachedLandingPad = false;
        }

        if(startLanding)
        {
            time += Time.deltaTime;
            spaceShip.transform.Rotate(0, ((180 ) / LandingTime) * Time.deltaTime, 0);
            if (chairWrapper != null)
            {
                chairWrapper.ConvertedRotation = chairWrapper.ConvertQuant2Euler(spaceShip.transform.rotation);
                chairWrapper.Yaw_factor = 1;
                chairWrapper.changeChairYaw();
                chairWrapper.DebugStatements(spaceShip);
            }
            spaceShip.transform.position = Vector3.MoveTowards(spaceShip.transform.position, LandingEndPosition, LandingSpeed * Time.deltaTime);
            //GameObject.Find("CameraRig").transform.Rotate(0,360/LandingTime * Time.deltaTime,0);
            if (Vector3.Distance(spaceShip.transform.position, LandingEndPosition) <= 2)
            {
                startLanding = false;
                PlayerPrefs.SetFloat("Scene_4_spaceship", spaceShip.transform.localEulerAngles.y);
            }  
        }
    }

    IEnumerator StartLanding()
    {
        yield return new WaitForSeconds(1.0f);

        quad.SetActive(true);
        display.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "You reached the landing pad!";
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("activating");
        yield return new WaitForSeconds(3f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);


        display.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Brace yourself for landing!";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("activating");
        yield return new WaitForSeconds(3f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        startLanding = true;
        //TODO: Call co-routine to imitate landing 
        if (chairWrapper != null)
        {
            //chairWrapper.Yaw_chair_angle -= 360;
            chairWrapper.Pitch_chair_angle -= 13;
            //VoyagerDevice.Yaw(new Vector3(chairWrapper.Yaw_chair_angle, 30f, 0));
            VoyagerDevice.Pitch(new Vector3(chairWrapper.Pitch_chair_angle, 1.08f, 1.08f));
            yield return new WaitForSeconds(LandingTime);
        }
        else
        {
            yield return new WaitForSeconds(LandingTime);
        }

        display.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Time taken... "+ sb.getFullScore();
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("activating");
        yield return new WaitForSeconds(1f);
        if (chairWrapper != null)
        {
            chairWrapper.Pitch_chair_angle = 5f;
            VoyagerDevice.Pitch(new Vector3(chairWrapper.Pitch_chair_angle, chairWrapper.Pitch_vel, chairWrapper.Pitch_acc));
        }
        yield return new WaitForSeconds(3f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);
        GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("Scene_4");
    }
}
