﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Positron;
using TMPro;
using System;

public class ChairInputWrapper : MonoBehaviour
{
    private TextMeshProUGUI text;

    //pitch by height
    private float prev_height;
    private float curr_height;
    private float delta_height;
    private float max_delta = 1f;
    private float max_pitch_angle = 10f;
    private float min_pitch_angle = 0f;

    //variables for yaw
    private float yaw_chair_angle = 0f;
    private float yaw_prev_angle = 0f;
    private float yaw_curr_angle = 0f;
    private float delta_yaw = 0f;
    private float yaw_vel = 50f;
    private float yaw_acc = 10f;
    private float yaw_factor = 1f;

    //variables for pitch
    private float pitch_chair_angle = 5f;
    private float pitch_prev_angle = 0f;
    private float pitch_curr_angle = 0f;
    private float delta_pitch = 0f;
    private float pitch_vel = 2f;
    private float pitch_acc = 2f;
    private float pitch_factor = 1f;

    private Vector3 convertedRotation;

    public float Pitch_chair_angle { get => pitch_chair_angle; set => pitch_chair_angle = value; }
    public float Yaw_chair_angle { get => yaw_chair_angle; set => yaw_chair_angle = value; }
    public Vector3 ConvertedRotation { get => convertedRotation; set => convertedRotation = value; }
    public float Delta_pitch { get => delta_pitch; set => delta_pitch = value; }
    public float Delta_yaw { get => delta_yaw; set => delta_yaw = value; }
    public float Yaw_prev_angle { get => yaw_prev_angle; set => yaw_prev_angle = value; }
    public float Yaw_curr_angle { get => yaw_curr_angle; set => yaw_curr_angle = value; }
    public float Pitch_prev_angle { get => pitch_prev_angle; set => pitch_prev_angle = value; }
    public float Pitch_curr_angle { get => pitch_curr_angle; set => pitch_curr_angle = value; }
    public float Pitch_factor { get => pitch_factor; set => pitch_factor = value; }

    private float prev_angle = 0;
    private float curr_angle = 0;
    public float Prev_angle { get => prev_angle; set => prev_angle = value; }
    public float Curr_angle { get => curr_angle; set => curr_angle = value; }
    public float Yaw_factor { get => yaw_factor; set => yaw_factor = value; }



    public float Prev_height { get => prev_height; set => prev_height = value; }
    public float Curr_height { get => curr_height; set => curr_height = value; }
    public float Max_delta { get => max_delta; set => max_delta = value; }


    public float Pitch_vel { get => pitch_vel; set => pitch_vel = value; }
    public float Pitch_acc { get => pitch_acc; set => pitch_acc = value; }


    // Start is called before the first frame update
    void Start()
    {
        // Set chair to default position
        if (PlayerPrefs.HasKey("Chair_Yaw"))
            yaw_chair_angle = PlayerPrefs.GetFloat("Chair_Yaw");
        if(PlayerPrefs.HasKey("Chair_Pitch"))
            pitch_chair_angle = PlayerPrefs.GetFloat("Chair_Pitch");

        VoyagerDevice.Yaw(new Vector3(Mathf.Floor(yaw_chair_angle), yaw_vel, yaw_acc));
        VoyagerDevice.Pitch(new Vector3(Mathf.Clamp(Mathf.Floor(pitch_chair_angle), -9, 25), pitch_vel, pitch_acc));
    }

    // Update is called once per frame

    void Update()
    {
    }


    public void changeChairYaw()
    {
        yaw_curr_angle = convertedRotation.y;

        if (yaw_prev_angle < 5 && yaw_curr_angle > 355)
        {
            delta_yaw = yaw_curr_angle - 360 - yaw_prev_angle;
        }
        else if (yaw_prev_angle > 355 && yaw_curr_angle < 5)
        {
            delta_yaw = yaw_curr_angle + 360 - yaw_prev_angle;
        }
        else
        {
            delta_yaw = yaw_curr_angle - yaw_prev_angle;
        }
        delta_yaw /= yaw_factor;
        yaw_chair_angle += delta_yaw;
        yaw_prev_angle = yaw_curr_angle;
        VoyagerDevice.Yaw(new Vector3(Mathf.Floor(yaw_chair_angle), yaw_vel, yaw_acc));
    }

    public void changeChairPitch()
    {
        pitch_curr_angle = convertedRotation.x;

        if (pitch_prev_angle < 5 && pitch_curr_angle > 355)
        {
            delta_pitch = pitch_curr_angle - 360 - pitch_prev_angle;
        }
        else if (pitch_prev_angle > 355 && pitch_curr_angle < 5)
        {
            delta_pitch = pitch_curr_angle + 360 - pitch_prev_angle;
        }
        else
        {
            delta_pitch = pitch_curr_angle - pitch_prev_angle;
        }
        delta_pitch /= pitch_factor;
        pitch_chair_angle -= delta_pitch;
        pitch_prev_angle = pitch_curr_angle;
        VoyagerDevice.Pitch(new Vector3(Mathf.Clamp(Mathf.Floor(pitch_chair_angle), -9, 25), pitch_vel, pitch_acc));
    }

    public void changeChairPitchByHeight(GameObject obj)
    {
        curr_height = obj.transform.localPosition.y;
        delta_height = (curr_height - prev_height);
        delta_angle = delta_height / max_delta;
        delta_angle = Mathf.Clamp(delta_angle, -2f, 2f);
        prev_height = curr_height;
        VoyagerDevice.Pitch(new Vector3(Mathf.Clamp(Mathf.Ceil(pitch_chair_angle + delta_angle), min_pitch_angle, max_pitch_angle), pitch_vel, pitch_acc));
    }

    private float delta_angle = 0;
    public void changeChairYaw1(GameObject ship)
    {
        yaw_curr_angle = convertedRotation.y;

        if (yaw_prev_angle < 5 && yaw_curr_angle > 355)
        {
            delta_yaw = yaw_curr_angle - 360 - yaw_prev_angle;
        }
        else if (yaw_prev_angle > 355 && yaw_curr_angle < 5)
        {
            delta_yaw = yaw_curr_angle + 360 - yaw_prev_angle;
        }
        else
        {
            delta_yaw = (yaw_curr_angle - yaw_prev_angle);
        }
        delta_yaw /= yaw_factor;
        //yaw_chair_angle += delta_yaw;
        yaw_prev_angle = yaw_curr_angle;
        curr_angle = -Vector3.SignedAngle(ship.transform.forward, GameObject.Find("CameraRig").transform.forward, Vector3.up);
        if (prev_angle > 0  && curr_angle < 0 && Mathf.Abs(curr_angle) > 170)
        {
                delta_angle = curr_angle + 360 - prev_angle;
        }
        else if (prev_angle < 0 && curr_angle > 0 && Mathf.Abs(curr_angle) > 170)
        {
            delta_angle = curr_angle - 360 - prev_angle;
        }
        else
        {
            delta_angle = curr_angle - prev_angle;
        }


        yaw_chair_angle += delta_angle;
        prev_angle = curr_angle;
        VoyagerDevice.Yaw(new Vector3(Mathf.Floor(yaw_chair_angle), yaw_vel, yaw_acc));
    }

    public void DebugStatements(GameObject ship)
    {
        if (text != null)
        {
            text.text = "Pitch Chair Angle:  " + pitch_chair_angle;
            text.text += "\n" + "Delta Pitch:  " + delta_pitch;
            text.text += "\n" + "Yaw Chair Angle:  " + yaw_chair_angle;
            text.text += "\n" + "Delta Yaw:  " + delta_yaw;
            text.text += "\n" + "Ship Rotation: " + ship.transform.eulerAngles;
            text.text += "\nCameraRig Rotation: " + GameObject.Find("CameraRig").transform.eulerAngles;
            text.text += "\nCamera Rotation: " + Camera.main.transform.eulerAngles;
            text.text += "\nShip Converted Rotation: " + convertedRotation;
            text.text += "\nAngle: " + -Vector3.SignedAngle(ship.transform.forward, GameObject.Find("CameraRig").transform.forward, Vector3.up);
            text.text += "\ndelta_angle: " + delta_angle;
        }
        else
        {
            text = GameObject.Find("Debug Logger").GetComponent<TextMeshProUGUI>();
        }
    }

    public void DebugStatementsMech(GameObject ship)
    {
        if (text != null)
        {
            text.text = "Pitch Chair Angle:  " + (pitch_chair_angle + delta_angle);
            text.text += "\n" + "Delta Pitch:  " + delta_angle;
            text.text += "\n" + "Yaw Chair Angle:  " + yaw_chair_angle;
            text.text += "\n" + "Delta Yaw:  " + delta_yaw;
            /*text.text += "\n" + "Ship Rotation: " + ship.transform.eulerAngles;
            text.text += "\nCameraRig Rotation: " + GameObject.Find("CameraRig").transform.eulerAngles;
            text.text += "\nCamera Rotation: " + Camera.main.transform.eulerAngles;
            text.text += "\nShip Converted Rotation: " + convertedRotation;
            text.text += "\nAngle: " + -Vector3.SignedAngle(ship.transform.forward, GameObject.Find("CameraRig").transform.forward, Vector3.up);
            text.text += "\ndelta_angle: " + delta_angle;*/
        }
        else
        {
            text = GameObject.Find("Debug Logger").GetComponent<TextMeshProUGUI>();
        }
    }

    public  Vector3 ConvertQuant2Euler(Quaternion quaternion)
    {
        float tempEuler;
        float[] eulerAngles = new float[3];

        //Convert pitch - X
        tempEuler = Mathf.Atan2(2 * quaternion.x * quaternion.w + 2 * quaternion.y * quaternion.z, 1 - 2 * quaternion.x * quaternion.x - 2 * quaternion.z * quaternion.z);
        eulerAngles[0] = tempEuler * 180 / Mathf.PI;

        //Convert YAW - Y
        tempEuler = Mathf.Atan2(2 * quaternion.y * quaternion.w + 2 * quaternion.x * quaternion.z, 1 - 2 * quaternion.y * quaternion.y - 2 * quaternion.z * quaternion.z);
        eulerAngles[1] = tempEuler * 180 / Mathf.PI;

        //Convert roll - z
        tempEuler = Mathf.Asin(2 * quaternion.x * quaternion.y + 2 * quaternion.z * quaternion.w);
        eulerAngles[2] = tempEuler * 180 / Mathf.PI;

        return new Vector3(eulerAngles[0] < 0 ? 360+ eulerAngles[0]:eulerAngles[0], eulerAngles[1] < 0 ? 360 + eulerAngles[1] : eulerAngles[1], eulerAngles[2] < 0 ? 360 + eulerAngles[2] : eulerAngles[2]);
    }
}
