﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Positron;
using UnityEngine.UI;


public class chairInputTester : MonoBehaviour
{
    public Text stateText;
    public Text configText;
    public Text deviceDataText;
    public Text lastRecvDataText;

    public bool invertPitch = true;
    public bool usingJoystick = true;
    [Range(-1, 1)]
    public float pitch;
    [Range(-1, 1)]
    public float yaw;
    [Range(-1, 1)]
    public float strafe;
    [Range(-1, 1)]
    public float throttle;

    public float threshold = 0.1f;

    private int multiplyerPitch = 1;
    

    private void Start()
    {

    }

    // Update is called once per frame
    public float pitchAngle=0f;
    public float yawAngle = 0f;
    void Update()
    {
        if (invertPitch)
            multiplyerPitch = -1;
        else
            multiplyerPitch = 1;
        if (!usingJoystick)
        {
            yaw = Input.GetAxis("Mouse X");
            pitch = Input.GetAxis("Mouse Y");
            //SetStickCommandsUsingMouse();
            strafe = Input.GetAxis("Horizontal");
            throttle = Input.GetAxis("Throttle");
        }
        else
        {
            pitch = Input.GetAxis("LeftJoyStickVertical");
            strafe = Input.GetAxis("LeftJoyStickHorizontal");
            yaw = Input.GetAxis("RightJoyStickHorizontal");
            throttle = Input.GetAxis("RightJoyStickVertical") * -1;

            yaw = Mathf.Abs(yaw) < threshold ? 0 : yaw;
            pitch = Mathf.Abs(pitch) < threshold ? 0 : pitch;
            pitch = Mathf.Clamp(pitch, -1.0f, 1.0f) * multiplyerPitch;
            yaw = Mathf.Clamp(yaw, -1.0f, 1.0f);
        }
        pitchAngle += pitch * Time.deltaTime * 10;
        pitchAngle = Mathf.Clamp(pitchAngle, -9, 15);

        yawAngle += yaw;

        string deviceStateStr;
        string lastRecvDataStr;
        VoyagerDeviceUtils.DevicePacketToJson(VoyagerDevice.deviceState, out deviceStateStr);
        VoyagerDeviceUtils.DevicePacketToJson(VoyagerDevice.LastRecvDevicePacket, out lastRecvDataStr);

        deviceDataText.text = deviceStateStr;
        lastRecvDataText.text = lastRecvDataStr;
        stateText.text = string.Format("Voyager state '{0}'", VoyagerDevice.PlayState.ToString());
    }

}
