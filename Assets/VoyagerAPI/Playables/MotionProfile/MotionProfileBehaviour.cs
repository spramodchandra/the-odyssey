using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class MotionProfileBehaviour : PlayableBehaviour
{
    public string profileName;
}
