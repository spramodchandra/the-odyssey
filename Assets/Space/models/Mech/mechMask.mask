%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: mechMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Bip001
    m_Weight: 0
  - m_Path: Bip001/Bip001 Footsteps
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 L Thigh
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 L Thigh/Bip001 L Calf
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 L Thigh/Bip001 L Calf/Bip001
      L Foot
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 L Thigh/Bip001 L Calf/Bip001
      L Foot/Bip001 L Toe0
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 L Thigh/Bip001 L Calf/Bip001
      L Foot/Bip001 L Toe0/Bip001 L Toe01
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 L Thigh/Bip001 L Calf/Bip001
      L Foot/Bip001 L Toe0/Bip001 L Toe01/Bip001 L Toe02
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 L Thigh/Bip001 L Calf/Bip001
      L Foot/Bip001 L Toe0/Bip001 L Toe01/Bip001 L Toe02/Bip001 L Toe0Nub
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 L Thigh/Bip001 L Calf/Bip001
      L Foot/foot
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 L Thigh/Bip001 L Calf/Leg
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 L Thigh/upperLeg
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Neck
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Neck/Bip001 Head
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Neck/Bip001 Head/Bip001 HeadNub
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Thigh
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Thigh/Bip001 R Calf
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Thigh/Bip001 R Calf/Bip001
      R Foot
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Thigh/Bip001 R Calf/Bip001
      R Foot/Bip001 R Toe0
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Thigh/Bip001 R Calf/Bip001
      R Foot/Bip001 R Toe0/Bip001 R Toe01
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Thigh/Bip001 R Calf/Bip001
      R Foot/Bip001 R Toe0/Bip001 R Toe01/Bip001 R Toe02
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Thigh/Bip001 R Calf/Bip001
      R Foot/Bip001 R Toe0/Bip001 R Toe01/Bip001 R Toe02/Bip001 R Toe0Nub
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Thigh/Bip001 R Calf/Bip001
      R Foot/footR
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Thigh/Bip001 R Calf/LegR
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Thigh/upperLegR
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/Circle001
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/Circle001/shoulderR
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/Circle001/shoulderR/upperArmR
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/Circle001/shoulderR/upperArmR/Circle003
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/Circle001/shoulderR/upperArmR/Circle003/upperArmRotatorR
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/Circle001/shoulderR/upperArmR/Circle003/upperArmRotatorR/lowerArmR
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/Circle001/shoulderR/upperArmR/Circle003/upperArmRotatorR/lowerArmR/handR
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/Circle001/shoulderR/upperArmR/Circle003/upperArmRotatorR/lowerArmR/handR/finger007
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/Circle001/shoulderR/upperArmR/Circle003/upperArmRotatorR/lowerArmR/handR/finger007/finger010
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/Circle001/shoulderR/upperArmR/Circle003/upperArmRotatorR/lowerArmR/handR/finger008
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/Circle001/shoulderR/upperArmR/Circle003/upperArmRotatorR/lowerArmR/handR/finger008/finger009
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/Circle001/shoulderR/upperArmR/Circle003/upperArmRotatorR/lowerArmR/handR/finger011
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/Circle001/shoulderR/upperArmR/Circle003/upperArmRotatorR/lowerArmR/handR/finger011/finger012
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/CircleL
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/CircleL/shoulder
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/CircleL/shoulder/upperArm
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/CircleL/shoulder/upperArm/CircleL
      1
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/CircleL/shoulder/upperArm/CircleL
      1/upperArmRotator
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/CircleL/shoulder/upperArm/CircleL
      1/upperArmRotator/lowerArm
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/CircleL/shoulder/upperArm/CircleL
      1/upperArmRotator/lowerArm/hand
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/CircleL/shoulder/upperArm/CircleL
      1/upperArmRotator/lowerArm/hand/finger1
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/CircleL/shoulder/upperArm/CircleL
      1/upperArmRotator/lowerArm/hand/finger1/finger2
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/CircleL/shoulder/upperArm/CircleL
      1/upperArmRotator/lowerArm/hand/finger003
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/CircleL/shoulder/upperArm/CircleL
      1/upperArmRotator/lowerArm/hand/finger003/finger004
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/CircleL/shoulder/upperArm/CircleL
      1/upperArmRotator/lowerArm/hand/finger005
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/CircleL/shoulder/upperArm/CircleL
      1/upperArmRotator/lowerArm/hand/finger005/finger006
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/cover
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/steerRight
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/cabin/steerRight001
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/mountPoint
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/pivot
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/sitpoint
    m_Weight: 0
