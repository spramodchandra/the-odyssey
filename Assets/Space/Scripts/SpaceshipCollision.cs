﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SpaceshipCollision : MonoBehaviour
{
    public int lives = 3;
    public Text LivesText;
    //public float startHealth = 1;
    //private float health;

    //public Image healthBar;
    // Start is called before the first frame update
    void Start()
    {
        LivesText = GameObject.Find("Lives").GetComponent<Text>();
        LivesText.text = "Lives:"+ lives.ToString();
        //health = startHealth;
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "asteroid")
        {
            Debug.Log("Collision detected");
            lives = lives - 1;
            //healthBar.fillAmount = 0;
            LivesText.text = "Lives:" + lives.ToString();
            if (lives <= 0)
            {

                LivesText.text = "Lives: 0";
                //Destroy(gameObject);
                SceneManager.LoadScene(0);


            }
            //else
            //{

            // }



        }
    }
    // Update is called once per frame
    void Update()
    {
      

    }
}
