﻿using UnityEngine;
using System.Collections;

public class MechControll : MonoBehaviour
{
    private Animator _animator;
    private AnimatorStateInfo currentBaseState;
    private CharacterController _charCtrl;
    private AudioSource m_AudioSource;
    public AudioClip m_Step;
    public AudioClip m_MechUp;
    private Vector3 moveDirection;//movement direction
    public bool activated;
    public bool canControl;
    private LayerMask characterMask = 5;
    private float _speed;
    private float _strafe;
    public float walkSpeed = 2f;
    public float rotationSpeed = 60f;//speed of rotating
    private float gravity = 4.0f;//gravity affecting character
    private float speed = 5.0f;//character's movement speed

    // Chair Integration
    private ChairInputWrapper chairWrapper;

    GameObject tracker;

    private int count = 0;
    public bool clearDeltaOnDisableControl = false;

    // Use this for initialization
    void Start()
    {
        _animator = GetComponent<Animator>();
        _charCtrl = GetComponent<CharacterController>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    private void Awake()
    {
        // get the car controller
        GameObject vm = GameObject.Find("Voyager Manager");
        tracker = GameObject.Find("Bip001");
        if (vm != null)
        {
            chairWrapper = vm.GetComponent<ChairInputWrapper>();
            
            chairWrapper.ConvertedRotation = chairWrapper.ConvertQuant2Euler(transform.rotation);
            chairWrapper.Yaw_factor = 3f;
            //chairWrapper.Pitch_prev_angle = chairWrapper.ConvertedRotation.x;
            chairWrapper.Yaw_prev_angle = chairWrapper.ConvertedRotation.y;
            chairWrapper.Prev_height = chairWrapper.Curr_height = tracker.transform.localPosition.y;
            chairWrapper.Max_delta = 0.05f;
            chairWrapper.Pitch_vel = 2f;
            chairWrapper.Pitch_acc = 2f;

            //chairWrapper.Pitch_factor = -2.5f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (canControl)
        {

            if (chairWrapper != null)
            {
                clearDeltaOnDisableControl = true;

                chairWrapper.ConvertedRotation = chairWrapper.ConvertQuant2Euler(transform.rotation);
                //chairWrapper.changeChairPitch();
                //chairWrapper.changeChairYaw1(this.gameObject);
                chairWrapper.changeChairYaw();
                if (count == 0)
                {
                    chairWrapper.changeChairPitchByHeight(tracker);
                }
                //chairWrapper.DebugStatementsMech(this.gameObject);
                count = (count+1) % 5;
            }
            _speed = Input.GetAxis("Vertical");//reading vertical axis input
            _strafe = Input.GetAxis("Horizontal");
            _speed = _speed > 0 ? 1 : _speed == 0 ? 0 : -1;
            _strafe = _strafe > 0 ? 1 : _strafe == 0 ? 0 : -1;

            if (_speed > 0)
            {
                _charCtrl.Move(transform.forward * walkSpeed * Time.deltaTime);
            }
            if (_speed < 0)
            {
                _charCtrl.Move(transform.forward * -walkSpeed * Time.deltaTime);
            }
            //////////ROTATING RIGHT
            if (_strafe > 0)
            {
                transform.Rotate(0, Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime, 0);
            }
            //////////ROTATING LEFT
            if (_strafe < 0)
            {
                transform.Rotate(0, Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime, 0);
            }
            _charCtrl.Move(moveDirection * speed * Time.deltaTime); //moving character controller forward with speed over time
            moveDirection.y -= gravity * Time.deltaTime;// applying gravity
        }
        else
        {
            _speed = 0;
            _strafe = 0;
            if (chairWrapper != null && clearDeltaOnDisableControl)
            {
                chairWrapper.Delta_pitch = 0;
                chairWrapper.Delta_yaw = 0;
                clearDeltaOnDisableControl = false;
            }
        }
        
        moveDirection.y -= gravity * Time.deltaTime;
        if (_charCtrl.enabled)
        {
            _charCtrl.Move(moveDirection * Time.deltaTime);
            if (moveDirection.y < -9f)
            {
                moveDirection.y = -9f;
            }
        }
    }
    void FixedUpdate()
    {
        _animator.SetFloat("Speed", _speed);
        _animator.SetFloat("Strafe", _strafe);
        _animator.SetBool("controll", activated);
        _animator.SetBool("Grounded", isGrounded());
        _animator.SetFloat("VertVelocity", _charCtrl.velocity.y);
        currentBaseState = _animator.GetCurrentAnimatorStateInfo(0);
    }
    bool isGrounded()
    {
        return Physics.CheckSphere(transform.position, 1f, characterMask | 1 << 9);
    }

    IEnumerator Stop()
    {
        yield return new WaitForSeconds(0.3f);
        rotationSpeed = 90;
        walkSpeed = 5;
    }
    //FOOTSTEP SOUND FUNCTION IS CALLED FROM ANIMATION EVENT 
    void footStep()
    {
        if (isGrounded())
        {
            m_AudioSource.PlayOneShot(m_Step);
        }
    }
    void MechUp()
    {
        if (isGrounded())
        {
            m_AudioSource.PlayOneShot(m_MechUp);
        }
    }

}
