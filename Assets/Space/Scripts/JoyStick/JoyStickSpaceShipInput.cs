﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoyStickSpaceShipInput : MonoBehaviour
{
    JoyStickSpaceShipControl spaceContrlScript;
    float threshold = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        spaceContrlScript = GetComponent<JoyStickSpaceShipControl>();
    }

    // Update is called once per frame
    void Update()
    {
        //float roll = Input.GetAxis ("Mouse X");
        float roll = Input.GetAxis("RightJoyStickHorizontal");
        //float pitch = Input.GetAxis("Mouse Y");
        float pitch = Input.GetAxis("LeftJoyStickVertical");
        bool airBrakes = Input.GetButton("Fire1");
        float throttle = Input.GetAxis("RightJoyStickVertical")*-1;

        Debug.Log(Input.GetButton("Up"));

        //Debug.Log("roll: " + roll + " pitch: " + pitch);
        roll = Mathf.Abs(roll) < threshold ? 0 : roll;
        pitch = Mathf.Abs(pitch) < threshold ? 0 : pitch;
        spaceContrlScript.Move(roll, pitch, 0, throttle, airBrakes, threshold);
    }
}
