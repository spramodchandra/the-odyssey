﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShipModelForCircle : MonoBehaviour
{
    private int count = 0;
    private GameObject destination;
    private float intensity = 0f;
    private float target = 1f;

    public GameObject Destination { get => destination; set => destination = value; }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (destination == null)
        {
            destination = GameObject.Find("FlyerGate");
            return;
        }
        else
        {
            if ((destination.transform.position - this.transform.position) == Vector3.zero)
                return;
            this.transform.forward = (destination.transform.position - this.transform.position).normalized;
            intensity = Mathf.MoveTowards(intensity, target, Time.deltaTime);
            if (intensity == 1)
                target = 0;
            if (intensity == 0)
                target = 1;
            this.GetComponent<Renderer>().sharedMaterial.SetFloat("_GlowIntensity", intensity);
        }
        //StartCoroutine(RotateImage());
        //StartCoroutine(RotateImage(-180));
        /*if (count == 0)
        {
            RotateModel(180,1f);
            if(this.transform.eulerAngles.y > 178)
            {
                count++;
            }
        }
        else if(count==1)
        {
            RotateModel(0,1f);
            if (this.transform.eulerAngles.y < 2)
            {
                count++;
            }
        }
        else if (count == 2)
        {
            RotateModel(90,1f);
            if (this.transform.eulerAngles.y > 89)
            {
                count++;
            }
        }
        else
        {
            this.transform.Rotate(0,Time.deltaTime * 50,0);
        }*/
    }

    IEnumerator RotateImage()
    {
        float Angle = 180;
        float moveSpeed = 0.0001f;
        float y = Angle;
        while (this.transform.rotation.y < Angle)
        {
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.Euler(0, Angle, 0), moveSpeed * Time.time);
            yield return null;
        }
        this.transform.rotation = Quaternion.Euler(0, Angle, 0);
        yield return null;
    }

    void RotateModel(int Angle, float speed)
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, Angle, 0), Time.deltaTime * speed);
    }
}
