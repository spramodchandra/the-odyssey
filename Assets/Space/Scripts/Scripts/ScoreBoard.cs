﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBoard : MonoBehaviour
{

    float startTime;
    float endTime;
    QuadController qc;
    public string msg;
    // Start is called before the first frame update
    void Start()
    {
        qc = GetComponent<QuadController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void startGame()
    {
        startTime = Time.time;
    }

    public void endGame()
    {
        endTime = Time.time;
    }

    public string getFullScore()
    {
        float completedTime = endTime - startTime;
        int mins = (int)(completedTime / 60);
        int secs = (int)(completedTime % 60);
        return mins + " mins " + secs + " secs";
    }

    public void getScore()
    {
        float completedTime = endTime - startTime;
        int mins = (int) (completedTime / 60);
        int secs = (int) (completedTime % 60);
        msg = mins + " mins " + secs + " secs";
        Debug.Log("Time taken: " + msg);
        StartCoroutine(qc.DisplayScore(msg));
        storeScore();
    }

    void storeScore()
    {
        List<float> scores;// = new List<int>();
        if(PlayerPrefs.HasKey("scores"))
        {
            scores = new List<float>(System.Array.ConvertAll(PlayerPrefs.GetString("scores").Split(','), float.Parse)); // PlayerPrefs.GetString("scores").Split(',');
        }
        else
        {
            scores = new List<float>();
        }
        scores.Add(endTime - startTime);
        GFG gg = new GFG();
        scores.Sort(gg);
        PlayerPrefs.SetString("scores", string.Join(",", scores.ToArray()));
        Debug.Log(PlayerPrefs.GetString("scores"));
        displayAllScores(scores, endTime - startTime);
    }

    void displayAllScores(List <float> scores, float playerScore)
    {
        string temp = "";
        int i = 1;
        foreach(float score in scores)
        {
            if (playerScore != score)
            {
                temp += i + ". " + score + "\n";
            }
            else
            {
                temp += i + ". " + score + "<--(You)\n";
            }
            i++;
        }
        //StartCoroutine(qc.DisplayScore(temp));
        Debug.Log(temp);
    }

    public void clearScore()
    {
        PlayerPrefs.DeleteKey("scores");
    }
}

class GFG : IComparer<float>
{
    public int Compare(float x, float y)
    {
        if (x == 0 || y == 0)
        {
            return 0;
        }

        // CompareTo() method 
        return x.CompareTo(y);

    }
}