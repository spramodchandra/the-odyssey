﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoSpaceShip1 : MonoBehaviour
{
    public float startTakeOffTime;
    public float endTakeOffTime;

    public float endForwardTime;

    public float endTime;
    public float forwardDoneTime = 16f;

    public bool isMainSpaceShip;

    public bool isRight;

    private int turnRight;

    private Rigidbody rigidBody;
    private Animator _anim;
    public bool landed;
    private float time;
    private Animator _animator;
    private bool takenOff;
    private bool animation;
    // Start is called before the first frame update
    void Start()
    {
        takenOff = false;
        animation = true;
        landed = true;
        _anim = this.transform.GetChild(0).GetComponent<Animator>();
        //_animator = GameObject.Find("spaceShip_Model").GetComponent<Animator>();
        rigidBody = this.GetComponent<Rigidbody>();
        turnRight = isRight ? 1 : -1;
        Debug.Log(rigidBody);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        time += Time.deltaTime;
        if (!takenOff && time > (startTakeOffTime) && time < endTakeOffTime)
            MoveSpaceShipUp();
        if(!takenOff && time > endTakeOffTime)
                takenOff = true;
        if(animation && takenOff &&!isMainSpaceShip)
        {
            _anim.SetBool("landing", false);
            _anim.SetBool("departure", true);
            /*if (isMainSpaceShip)
            {
                _animator.SetBool("landing", false);
                _animator.SetBool("departure", true);
            }*/
            animation = false;
        }

        if(time < endForwardTime && time > (endTakeOffTime + 1))
        {
            MoveSpaceShipForward();
        }

        if(!isMainSpaceShip && time > (endTakeOffTime + 4) && time < endForwardTime)
        {
            if(isRight)
                rigidBody.AddTorque(transform.up * 0.015f * turnRight);
            else
                rigidBody.AddTorque(transform.up * 0.010f * turnRight);
        }

        if(isMainSpaceShip && time > endForwardTime)
        {
            Transform spaceShip = this.transform.GetChild(0);
            spaceShip.GetComponent<Rigidbody>().isKinematic = false;
            spaceShip.GetComponent<Rigidbody>().useGravity = false;
            spaceShip.GetComponent<SpaceShipControl>().landed = false;
            spaceShip.GetComponent<SpaceShipControl>().canControl =  true;
            spaceShip.transform.parent = null;
            Destroy(this.gameObject);
        }

        if(!isMainSpaceShip && time > forwardDoneTime && time < forwardDoneTime + endTime)
        {
            rigidBody.AddForce(transform.forward * Time.deltaTime * 13000 * rigidBody.mass);
            if (isRight)
                rigidBody.AddTorque(transform.up * 0.10f * turnRight);
            else
                rigidBody.AddTorque(transform.up * 0.10f * turnRight);
            
        }
        if(!isMainSpaceShip && time > forwardDoneTime + endTime)
        {
            Destroy(this.gameObject);
        }

    }

    void MoveSpaceShipUp()
    {
        //Debug.Log("Moving Space Ship Up "+ (transform.up * Time.deltaTime * 15000 * rigidBody.mass));
        rigidBody.AddForce(transform.up * Time.deltaTime * 150 * rigidBody.mass);
        if (landed)
        {
            //Debug.Log("landed");
            rigidBody.useGravity = false;
            //canControl = true;
            landed = false;
            _anim.SetBool("landing", false);
            _anim.SetBool("departure", true);
        }
    }

    void MoveSpaceShipForward()
    {
        //Debug.Log("Moving Space Ship Up "+ (transform.up * Time.deltaTime * 15000 * rigidBody.mass));
        rigidBody.AddForce(transform.forward * Time.deltaTime * 1500 * rigidBody.mass);
    }

    void RotateSpaceShip()
    {
        rigidBody.AddTorque(transform.forward * 500);
    }
}
