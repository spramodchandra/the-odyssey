﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoSpaceShip2 : MonoBehaviour
{
    // Start is called before the first frame update
    public float startTakeOffTime;
    public float endTakeOffTime;

    public float endForwardTime;

    public float endTime;
    public float forwardDoneTime = 16f;

    public bool isMainSpaceShip;

    public bool isRight;

    private int turnRight;

    private Rigidbody rigidBody;
    private Animator _anim;
    public bool landed;
    private float time;
    private Animator _animator;
    private bool takenOff;
    private bool animation;
    // Start is called before the first frame update
    void Start()
    {
        takenOff = false;
        animation = true;
        landed = true;
        for (int i = 0; i < this.transform.childCount; i++)
        {
            _anim = this.transform.GetChild(i).GetComponent<Animator>();
            _anim.SetBool("landing", false);
            _anim.SetBool("departure", true);
        }
        rigidBody = this.GetComponent<Rigidbody>();
        turnRight = isRight ? 1 : -1;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (!takenOff && time > (startTakeOffTime) && time < endTakeOffTime)
            MoveSpaceShipUp(5000f);
        if (time > endTakeOffTime && time < endTakeOffTime + 5f)
            MoveSpaceShipUp(50000f);
        if (time > endTakeOffTime + 5f)
            Destroy(this.gameObject);
    }

    void MoveSpaceShipUp(float speed)
    {
        //Debug.Log("Moving Space Ship Up "+ (transform.up * Time.deltaTime * 15000 * rigidBody.mass));
        rigidBody.AddForce(transform.forward * Time.deltaTime * speed * rigidBody.mass);
        if (landed)
        {
            //Debug.Log("landed");
            rigidBody.useGravity = false;
            //canControl = true;
            landed = false;
            
        }
    }
}
