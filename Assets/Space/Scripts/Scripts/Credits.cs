﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour
{

    GameObject asteroid;
    GameObject spaceShip;
    public float speed = 1000f;
    int count = 0;
    // Start is called before the first frame update
    void Start()
    {
        spaceShip = GameObject.Find("spaceShip");
        asteroid = GameObject.Find("asteroidA");
        if (GameObject.Find("UniversalSoundObject"))
        {
            AudioSource[] universalSoundObject = GameObject.Find("UniversalSoundObject").GetComponents<AudioSource>();
            universalSoundObject[2].Play();
        }
    }

    // Update is called once per frame
    void Update()
    {
        /*if(count == 0)
            asteroid.GetComponent<Rigidbody>().AddForce((spaceShip.transform.position - asteroid.transform.position).normalized * speed * Time.deltaTime);
        else if(count == 1)
        {
            //StartCoroutine();
        }*/
    }

    private void OnCollisionEnter(Collision collision)
    {
        if( count == 0 && collision.gameObject.name.Contains("Asteroid") )
        {
            count++;
        }
    }

    public void ChangeScene()
    {
        GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("Scene_1");
    }

}
