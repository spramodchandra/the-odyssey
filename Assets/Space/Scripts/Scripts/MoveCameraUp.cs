﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCameraUp : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject mech;
    Vector3 lastPosition;
    Vector3 currentPosition;

    float height;

    void Start()
    {
        mech = GameObject.Find("Bip001");
        lastPosition = mech.transform.position;
        height = transform.position.y - mech.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x, mech.transform.position.y+height, transform.position.z);
        /*currentPosition = mech.transform.position;
        transform.position += (currentPosition-lastPosition);
        lastPosition = currentPosition;
        StartCoroutine(Delete());*/
    }

    IEnumerator Delete()
    {
        yield return new WaitForSeconds(2);
        GameObject.Find("Mech").GetComponent<MechControll>().canControl = true;
        //Destroy(this);
    }
}
