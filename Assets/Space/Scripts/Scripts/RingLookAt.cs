﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingLookAt : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject target;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(target.transform.position , transform.position) > 50f)
            transform.rotation = Quaternion.LookRotation(target.transform.position - transform.position , Vector3.up);
    }
}
