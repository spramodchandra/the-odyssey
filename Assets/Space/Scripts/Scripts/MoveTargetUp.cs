﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTargetUp : MonoBehaviour
{
    // Start is called before the first frame update
    Vector3 targetPosition;
    Rigidbody rigidbody;

    void Start()
    {
        targetPosition = gameObject.transform.position + new Vector3(0, 400f, 0);
        rigidbody = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, targetPosition) > 1f)
        { 
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, 10 * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(10,transform.localEulerAngles.y,0), Time.deltaTime);
        }
        else
        {
            rigidbody.AddForce(transform.forward * Time.deltaTime * 50000 * rigidbody.mass);
        }
    }
}
