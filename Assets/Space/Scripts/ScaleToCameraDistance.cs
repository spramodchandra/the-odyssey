﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleToCameraDistance : MonoBehaviour
{
    [SerializeField]
    private float minDistace = 5000f;
    private float startingDistance;
    private float maxDistance;
    private List<GameObject> celestialBodies = new List<GameObject>();
    List<Vector3> originalScale = new List<Vector3>();
    private float multiplyer;
    // Start is called before the first frame update
    void Start()
    {
        celestialBodies.AddRange(GameObject.FindGameObjectsWithTag("Planet"));
        GameObject sun = GameObject.Find("sun");
        if(sun!=null)
        celestialBodies.Add(sun);
        maxDistance = Camera.main.farClipPlane + 10000;
        multiplyer = maxDistance - minDistace;
        foreach (GameObject obj in celestialBodies)
        {
            originalScale.Add(obj.transform.localScale);
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < celestialBodies.Count; i++)
        {
            GameObject obj = celestialBodies[i];
            float curDistance = Vector3.Distance(Camera.main.transform.position, obj.transform.position);
            if (curDistance <= minDistace)
                obj.transform.localScale = originalScale[i];
            else if (curDistance > maxDistance)
                obj.transform.localScale = new Vector3(2, 2, 2);
            else
            {
                obj.transform.localScale = originalScale[i] * (maxDistance - curDistance) / multiplyer;
            }
        }
    }
}
