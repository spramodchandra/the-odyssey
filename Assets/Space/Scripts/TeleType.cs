﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TeleType : MonoBehaviour
{
    private TextMeshProUGUI tmp;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Type());
    }

    public IEnumerator Type()
    {
        tmp = gameObject.GetComponent<TextMeshProUGUI>();
        int totalchar = tmp.text.Length;
        int counter = 0;
        while (true)
        {
            int visiblecount = counter % (totalchar + 1);
            tmp.maxVisibleCharacters = visiblecount;
            if (visiblecount >= totalchar)
            {
                yield return new WaitForSeconds(1.0f);
                break;
            }

            counter += 1;
            yield return new WaitForSeconds(0.05f);
        }
    }

}
