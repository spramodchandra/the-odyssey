﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRCameraReset : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (UnityEngine.XR.XRDevice.isPresent)
        {
            UnityEngine.XR.XRDevice.SetTrackingSpaceType(UnityEngine.XR.TrackingSpaceType.Stationary);
            UnityEngine.XR.InputTracking.Recenter();
        }
        if (Valve.VR.OpenVR.IsHmdPresent())
        {
            Valve.VR.OpenVR.System.ResetSeatedZeroPose();
            Valve.VR.OpenVR.Compositor.SetTrackingSpace(Valve.VR.ETrackingUniverseOrigin.TrackingUniverseSeated);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
