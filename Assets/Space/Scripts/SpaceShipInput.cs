﻿using UnityEngine;
using System.Collections;

public class SpaceShipInput : MonoBehaviour {
	SpaceShipControl spaceContrlScript;
    SpaceShipEngine spaceEngineScript;
    public bool usingJoystick = true;
    public float threshold = 0.1f;
    // Use this for initialization
    void Start () {
		spaceContrlScript = GetComponent<SpaceShipControl>();
        spaceContrlScript.usingJoystick = usingJoystick;

        spaceEngineScript = GetComponent<SpaceShipEngine>();
        spaceEngineScript.setUsingJoyStick(usingJoystick);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (!usingJoystick)
        {
            float roll = Input.GetAxis("Mouse X");
            float pitch = Input.GetAxis("Mouse Y");
            bool airBrakes = Input.GetButton("Fire1");
            float throttle = Input.GetAxis("Throttle");
            spaceContrlScript.Move(roll, pitch, 0, throttle, airBrakes, 0);
        }
        else
        {
            float roll = Input.GetAxis("RightJoyStickHorizontal");
            float pitch = Input.GetAxis("LeftJoyStickVertical");
            bool airBrakes = Input.GetButton("Fire1");
            float throttle = Input.GetAxis("RightJoyStickVertical") * -1;
            roll = Mathf.Abs(roll) < threshold ? 0 : roll;
            pitch = Mathf.Abs(pitch) < threshold ? 0 : pitch;
            spaceContrlScript.Move(roll, pitch, 0, throttle, airBrakes, threshold*3);
        }
		
	}
}
