﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HealthBar : MonoBehaviour
{
    private Image healthBar;
    private int prev_coll = 0;
    // Start is called before the first frame update
    void Start()
    {
        //healthBar = this.GetComponentInChildren<Image>();
        healthBar = GameObject.Find("healthBar").GetComponent<Image>();

    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "asteroid")
        {
            if (other.gameObject.GetInstanceID() != prev_coll)
            {
                prev_coll = other.gameObject.GetInstanceID();
                Debug.Log("Collision detected from healthbar");
                Debug.Log(healthBar);
                healthBar.fillAmount -= 0.333f;
                if (healthBar.fillAmount == 0)
                {
                    SceneManager.LoadScene(0);
                }
            }    
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
