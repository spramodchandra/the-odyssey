﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Positron;
using UnityEngine.EventSystems;
public class MainMenu : MonoBehaviour
{
    private bool play = false;
    private GameObject ships;
    private Rigidbody rigidBody;
    private float time = 0f;
    private ParticleSystem starDust;
    private float speed = 1000f;
    private float length = 2f;
    private GameObject MainScreen;
    public float endTime = 2.0f;
    public float hyperSpeedEndTime = 2.0f;
    public string selection = "default";
    private int trigger = 0;


    private bool gotQuad = false;
    private GameObject spaceShip;


    private QuadController qc;

    private GameObject leftShip;
    private GameObject rightShip;
    private AudioSource bgm;


    private GameObject menuButtons;
    private GameObject loadButtons;
    private GameObject optionButtons;
    private GameObject difficultyButtons;
    private GameObject headTrackingButtons;
    private GameObject title;
    private AudioSource[] universalSoundObject;
    private void Start()
    {
        spaceShip = GameObject.Find("spaceShip");
        leftShip = GameObject.Find("ship1");
        rightShip = GameObject.Find("ship2");
        qc = spaceShip.GetComponent<QuadController>();
        bgm = GameObject.Find("MainScreen").GetComponent<AudioSource>();
        menuButtons = GameObject.Find("MenuScreenButtons");
        loadButtons = GameObject.Find("LoadScreenButtons");
        optionButtons = GameObject.Find("OptionScreenButtons");
        difficultyButtons = GameObject.Find("DifficultyButtons");
        headTrackingButtons = GameObject.Find("HeadPositionTrackingButtons");
        loadButtons.active = false;
        optionButtons.active = false;
        difficultyButtons.active = false;
        headTrackingButtons.active = false;
        if (!PlayerPrefs.HasKey("Head_Position"))
            PlayerPrefs.SetString("Head_Position", "True_Head");

        if (!PlayerPrefs.HasKey("Difficulty"))
            PlayerPrefs.SetString("Difficulty", "Easy");
        title = GameObject.Find("Title");
        universalSoundObject = GameObject.Find("UniversalSoundObject").GetComponents<AudioSource>();
    }

    public void PlayGame()
    {
        CanvasGroup menuscreen = this.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(menuscreen, menuscreen.alpha, 0, .5f));
        play = true;
        ships = GameObject.Find("Ships");

        starDust = GameObject.Find("Speed Dust").GetComponent<ParticleSystem>();
        starDust.Stop();
        MainScreen = GameObject.Find("MainScreen");
        bgm.enabled = false;
    }

    private bool showButtons = false;
    private float titleSpeed = 0f;
    private void Update()
    {
        titleSpeed = Mathf.MoveTowards(titleSpeed, 5, Time.deltaTime );
        title.transform.localPosition = Vector3.MoveTowards(title.transform.localPosition, Vector3.zero, titleSpeed);

        if(Vector3.Distance(title.transform.localPosition,Vector3.zero)==0 && !showButtons)
        {
            StartCoroutine(FadeCanvasGroup(menuButtons.GetComponent<CanvasGroup>(), 0, 1, .05f));
            showButtons = true;
        }
        if (play)
        {
            if (Input.GetButton("ShipUp")|| Input.GetKeyUp(KeyCode.Tab))
            {
                GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("Scene_2");
            }
            starDust.GetComponent<ParticleSystemRenderer>().renderMode = ParticleSystemRenderMode.Stretch;
            time += Time.deltaTime;

            if (time < endTime)
            {
                length = Mathf.MoveTowards(length, 10, Time.deltaTime);
                starDust.GetComponent<ParticleSystemRenderer>().lengthScale = length;
                starDust.Play();
            }
            else if (time > hyperSpeedEndTime)
            {
                length = Mathf.MoveTowards(length, 0, Time.deltaTime * 200);
                starDust.GetComponent<ParticleSystemRenderer>().lengthScale = length;
                starDust.Play();
                StartCoroutine(MoveShip(leftShip, true));
                StartCoroutine(MoveShip(rightShip, false));
            }
            else
            {
                length = Mathf.MoveTowards(length, 1000, Time.deltaTime * 300);
                starDust.GetComponent<ParticleSystemRenderer>().lengthScale = length;
                starDust.Play();
            }

            if (MainScreen != null)
            {
                if (time < endTime)
                {
                    speed = Mathf.MoveTowards(speed, 0.1f, Time.deltaTime * 0.1f);
                    MainScreen.transform.Translate(MainScreen.transform.right * Time.deltaTime * -1 * speed * 0.1f);
                }
                else
                {
                    speed = Mathf.MoveTowards(speed, 1f, Time.deltaTime * 1f);
                    MainScreen.transform.Translate(MainScreen.transform.right * Time.deltaTime * -1 * speed * 1f);
                }
                if (!gotQuad && time > endTime + 3f)
                {
                    StartCoroutine(qc.BroadCastInitialMessage());
                    gotQuad = true;
                }

                if (time > hyperSpeedEndTime && length <= 10)
                {
                    SceneManager.LoadScene(1);
                }
            }

        }

        if (EventSystem.current && EventSystem.current.currentSelectedGameObject && !EventSystem.current.currentSelectedGameObject.name.Equals(selection) && showButtons)
        {
            universalSoundObject[0].Play();
            selection = EventSystem.current.currentSelectedGameObject.name;  
        }


    }

    public void QuitGame()
    {
        Debug.Log("Quit");
        VoyagerDevice.Stop();
    }

    public void Options()
    {
        CanvasGroup menuScreenCanvas = menuButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(menuScreenCanvas, menuScreenCanvas.alpha, 0, .5f));
        optionButtons.active = true;
        menuButtons.active = false;
        CanvasGroup optionScreenCanvas = optionButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(optionScreenCanvas, 0, 1, .5f));
        EventSystem.current.SetSelectedGameObject(GameObject.Find("Difficulty"));
    }

    public void Back_Options()
    {
        CanvasGroup optionScreenCanvas = optionButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(optionScreenCanvas, optionScreenCanvas.alpha, 0, .5f));
        menuButtons.active = true;
        optionButtons.active = false;
        CanvasGroup menuScreenCanvas = menuButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(menuScreenCanvas, 0, 1, .5f));
        EventSystem.current.SetSelectedGameObject(GameObject.Find("Options"));
    }

    public void Difficulty()
    {
        CanvasGroup optionScreenCanvas = optionButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(optionScreenCanvas, optionScreenCanvas.alpha, 0, .5f));
        difficultyButtons.active = true;
        optionButtons.active = false;
        CanvasGroup difficultyCanvas = difficultyButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(difficultyCanvas, 0, 1, .5f));
        if (PlayerPrefs.HasKey("Difficulty"))
        {
            EventSystem.current.SetSelectedGameObject(GameObject.Find(PlayerPrefs.GetString("Difficulty")));
        }
        else
        {
            EventSystem.current.SetSelectedGameObject(GameObject.Find("Easy"));
        }
    }
    public void DifficultySelected()
    {
        PlayerPrefs.SetString("Difficulty", EventSystem.current.currentSelectedGameObject.name);
        CanvasGroup difficultyCanvas = difficultyButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(difficultyCanvas, difficultyCanvas.alpha, 1, .5f));
        optionButtons.active = true;
        difficultyButtons.active = false;
        CanvasGroup optionScreenCanvas = optionButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(optionScreenCanvas, 0, 1, .5f));
        EventSystem.current.SetSelectedGameObject(GameObject.Find("Difficulty"));
    }

    public void Tutorial()
    {
        GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("Scene_Tutorial");
    }

    public void Load()
    {
        CanvasGroup menuScreenCanvas = menuButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(menuScreenCanvas, menuScreenCanvas.alpha, 0, .5f));
        loadButtons.active = true;
        menuButtons.active = false;
        CanvasGroup loadScreenCanvas = loadButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(loadScreenCanvas, 0, 1, .5f));
        EventSystem.current.SetSelectedGameObject(GameObject.Find("Space"));
    }
    public void Space()
    {
        GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("Scene_2");
    }
    public void Landing()
    {
        GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("Scene_3");
    }
    public void Planet()
    {
        GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("Scene_4");
    }

    public void Back()
    {
        CanvasGroup loadScreenButtons = loadButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(loadScreenButtons, loadScreenButtons.alpha, 0, .5f));
        menuButtons.active = true;
        loadButtons.active = false;
        CanvasGroup menuScreenButtons = menuButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(menuScreenButtons, 0, 1, .5f));
        EventSystem.current.SetSelectedGameObject(GameObject.Find("Load"));
    }
    public void HeadTracking()
    {
        CanvasGroup optionScreenCanvas = optionButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(optionScreenCanvas, optionScreenCanvas.alpha, 0, .5f));
        headTrackingButtons.active = true;
        optionButtons.active = false;
        CanvasGroup headCanvas = headTrackingButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(headCanvas, 0, 1, .5f));
        if (PlayerPrefs.HasKey("Head_Position"))
        {
            EventSystem.current.SetSelectedGameObject(GameObject.Find(PlayerPrefs.GetString("Head_Position")));
        }
        else
        {
            EventSystem.current.SetSelectedGameObject(GameObject.Find("False_Head"));
        }

    }
    public void ChangeHeadTrackingOption()
    {
        string option = EventSystem.current.currentSelectedGameObject.name;
        if(option == "True_Head")
            UnityEngine.XR.InputTracking.disablePositionalTracking = false;
        else
            UnityEngine.XR.InputTracking.disablePositionalTracking = true;
        PlayerPrefs.SetString("Head_Position", option);

        CanvasGroup headCanvas = headTrackingButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(headCanvas, headCanvas.alpha, 1, .5f));
        optionButtons.active = true;
        headTrackingButtons.active = false;
        CanvasGroup optionScreenCanvas = optionButtons.GetComponent<CanvasGroup>();
        StartCoroutine(FadeCanvasGroup(optionScreenCanvas, 0, 1, .5f));
        EventSystem.current.SetSelectedGameObject(GameObject.Find("HeadTracking"));
    }


    public IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 1)
    {
        float _timeStartedLerping = Time.time;
        float timeSinceStarted = Time.time - _timeStartedLerping;
        float percentageComplete = timeSinceStarted / lerpTime;

        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            float currentValue = Mathf.Lerp(start, end, percentageComplete);

            cg.alpha = currentValue;

            if (percentageComplete >= 1) break;

            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator MoveShip(GameObject go, bool left)
    {
        if (go.name == "ship2")
        {
            go.GetComponent<Animator>().enabled = false;
        }
        rigidBody = go.GetComponent<Rigidbody>();
        int invert = left ? -1 : 1;
        rigidBody.AddForce(transform.forward * Time.deltaTime * 7000 * rigidBody.mass);
        rigidBody.AddTorque(transform.up * Time.deltaTime * 7 * invert);
        yield return null;
    }


    IEnumerator MoveTitle()
    {

        yield return null;
    }
}
