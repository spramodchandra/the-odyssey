﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeBar : MonoBehaviour
{
    public Ship ship;
    public float time = 1f;
    public float factor = 0.1f;
    float maxScale;
    GameObject cylinder;
    private bool ShowGameOver = false;
    // Start is called before the first frame update
    void Start()
    {
        cylinder = GameObject.Find("Cylinder");
        maxScale = cylinder.transform.localScale.y;
        ship = GameObject.Find("spaceShip").GetComponent<Ship>();
    }

    // Update is called once per frame
    float endTime = 0;
    void Update()
    {
        if (ShowGameOver)
        {
            endTime += Time.deltaTime;
            if (endTime > 5.0f)
            {
                GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("Scene_1");
            }
            return;
        }
        if (!ship.canControl)
            return;

        time = time - Time.deltaTime/factor;
        if (time <= 0)
        {
            //end of game
            ShowGameOver = true;
            StartCoroutine(gameover());
            ship.GetComponent<Ship>().canControl = false;
            time = 0f;
        }
        Vector3 currentScale = cylinder.transform.localScale;

        float newY = time * maxScale;
        float diff = newY - cylinder.transform.localScale.y;

        cylinder.transform.localPosition += new Vector3(0f, diff, 0f);
        cylinder.transform.localScale = new Vector3(currentScale.x, newY, currentScale.z);
    }

    IEnumerator gameover()
    {
        foreach (Transform child in GameObject.Find("cockpit").GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "GameOver")
            {
                child.gameObject.SetActive(true);
                yield return new WaitForSeconds(2.0f);
            }
        }
    }
}
