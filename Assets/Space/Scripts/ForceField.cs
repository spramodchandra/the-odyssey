﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceField : MonoBehaviour
{
    private GameObject forcefield;
    private int prev_coll = 0;
    // Start is called before the first frame update
    void Start()
    {
        forcefield = GameObject.Find("ForceField");
    }
    IEnumerator Flicker()
    {
        forcefield = GameObject.Find("ForceField");
        Debug.Log("Collision: Flicking entered");
        forcefield.GetComponent<MeshRenderer>().enabled = true;
        Debug.Log("Collision: Flicking start Loop");
        for (int i = 0; i < 4; i++)
        {
            Debug.Log("Collision: Flicking");
            yield return new WaitForSeconds(0.45f);
            forcefield.GetComponent<MeshRenderer>().enabled = !forcefield.GetComponent<MeshRenderer>().enabled;
        }
        forcefield.GetComponent<MeshRenderer>().enabled = false;
        Debug.Log("Collision: Flicking End");
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "asteroid")
        {
            if (other.gameObject.GetInstanceID() != prev_coll)
            {
                prev_coll = other.gameObject.GetInstanceID();
                Debug.Log("Collision detected from healthbar");


                //forcefield.GetComponent<MeshRenderer>().enabled = true;
                //FadeOut();
                StartCoroutine(Flicker());
            }
        }
    }
                // Update is called once per frame
                void Update()
    {
        
    }
}
