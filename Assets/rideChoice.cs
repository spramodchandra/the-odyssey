﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rideChoice : MonoBehaviour
{
    private GameObject mech_tiny;
    private GameObject rider_tiny;
    private Animator animator;
    private AudioSource[] universalSoundObject=null;
    private string selection = "Mech Button";
    // Start is called before the first frame update
    void Start()
    {
        UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(GameObject.Find("Mech Button"));
        mech_tiny = GameObject.Find("mech_tiny");
        rider_tiny = GameObject.Find("rider_tiny");
        animator = GameObject.Find("ScreenFader").GetComponent<Animator>();
        GameObject uso = GameObject.Find("UniversalSoundObject");

        if (uso != null)
            universalSoundObject = uso.GetComponents<AudioSource>();
    }

    // Update is called once per frame
    private float target = 1;
    private float target1 = 1;

    void Update()
    {
        if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject == null)
            return;

        if (!selection.Equals(UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name))
        {
            selection = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;
            if (universalSoundObject != null)
                universalSoundObject[0].Play();
        }

        if(UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name == "Mech Button")
        {
            target = Mathf.MoveTowards(target, 1.5f, Time.deltaTime);
            target1 = Mathf.MoveTowards(target1, 1f, Time.deltaTime);
            mech_tiny.transform.localScale = Vector3.one * target;
            rider_tiny.transform.localScale = Vector3.one * target1;
        }
        else
        {
            target = Mathf.MoveTowards(target, 1f, Time.deltaTime);
            target1 = Mathf.MoveTowards(target1, 1.5f, Time.deltaTime);
            mech_tiny.transform.localScale = Vector3.one * target;
            rider_tiny.transform.localScale = Vector3.one * target1;
        }
        mech_tiny.transform.Rotate(0, Time.deltaTime * 50, 0);
        rider_tiny.transform.Rotate(0, Time.deltaTime * 50, 0);
    }

    public void Rover()
    {
        StartCoroutine(MoveToVehicle(GameObject.Find("CameraPositionRider"),"rider"));
    }
    public void Mech()
    {
        StartCoroutine(MoveToVehicle(GameObject.Find("CameraPositionMech"), "Mech"));
    }
    public void cleanup()
    {
        UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
        GameObject rcm = GameObject.Find("RiderChoiceMenu");
        if(rcm!=null)
            rcm.active = false;
        this.enabled = false;
    }

    public IEnumerator MoveToVehicle(GameObject obj,string vehicle)
    {

        animator.SetTrigger("Out");
        yield return new WaitForSeconds(1f);
        cleanup();
        yield return new WaitForSeconds(1f);

        GameObject camera = GameObject.Find("CameraRig");
        camera.transform.parent = obj.transform;
        camera.transform.localPosition = Vector3.zero;
        camera.transform.rotation = camera.transform.parent.rotation;
        
        animator.SetTrigger("In");
        yield return new WaitForSeconds(1f);

        if(vehicle == "rider")
        {
            GameObject.Find("rider").GetComponent<UnityStandardAssets.Vehicles.Car.CarUserControl>().canControl = true;
        }
        else
        {
            GameObject.Find("Mech").GetComponent<MechControll>().activated = true;
            GameObject.Find("Mech").GetComponent<MechControll>().canControl = true;
        }
        animator.SetTrigger("End");
        if (GameObject.Find("UniversalSoundObject"))
        {
            AudioSource[] universalSoundObject = GameObject.Find("UniversalSoundObject").GetComponents<AudioSource>();
            universalSoundObject[1].Play();
        }
    }
}
